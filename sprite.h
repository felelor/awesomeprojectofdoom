#ifndef SPRITE_H
#define SPRITE_H
#include <SDL/SDL.h>
//#include "Tower.h"
#include <string>
#include "Tile.h"
#include <vector>
using namespace std;

class sprite {

	public:
		sprite();
		//void draw_sprite(SDL_Surface*,SDL_Surface*,int, int, int, int, int, int); //sprite drawing fnc
		int check_key(int,int,int,Tile);
		void play();
		void animate(int,int,SDL_Surface*,SDL_Surface*,SDL_Surface*,Tile);
		SDL_Surface* load_image( std::string filename );
		SDL_Surface* flip_image(SDL_Surface*, int); // flips image for towers that are on other side of path
		Uint32 get_pixel32( SDL_Surface*, int, int );
		void put_pixel32( SDL_Surface* , int, int, Uint32 );

	private:
		int sprite_x;
		int sum;
		int go_time;
		int sprite_y;
		int state;
		int tower_type;
		int evolution;
		int why, ex, x_hell, y_hell;
		vector<char > direction;
		vector<int> x_proj, y_proj;
		vector<SDL_Rect> enemy_home;
		vector<vector<char> > enemy_conglom;
		//Tile Tester;
		//Tower tower1;
		//Tower tower2;
		//Tower tower3;
		//SDL_Surface* sprite_sheet;
		//SDL_Surface* screen = SDL_SetVideoMode(WINDOW_WIDTH, WINDOW_HEIGHT,0,SDL_HWSURFACE | SDL_DOUBLEBUF);
};
//SDL_Surface*,SDL_Surface*

#endif
