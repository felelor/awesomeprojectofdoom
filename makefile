all:	main

main: Tower.o node_Tile.o Typeset.o Node.o Timer.o oldsprite.o oldmain.o
	g++ Tower.o node_Tile.o Typeset.o Node.o Timer.o oldsprite.o oldmain.o -o oldmain -lSDL -lSDL_ttf

Tower.o: Tower.cpp
	g++ -c Tower.cpp -lSDL

node_Tile.o: node_Tile.cpp
	g++ -c node_Tile.cpp -lSDL

Typeset.o: Typeset.cpp
	g++ -c Typeset.cpp -lSDL

Node.o: Node.cpp
	g++ -c Node.cpp

Timer.o: Timer.cpp
	g++ -c Timer.cpp -lSDL 

oldsprite.o: oldsprite.cpp
	g++ -c oldsprite.cpp -lSDL -lSDL_ttf

oldmain.o: oldmain.cpp
	g++ -c oldmain.cpp -lSDL

clean:
	rm -f *.o main *gch
