#include<vector>
#include <iostream>
#include <Actor.h>
using namespace std;

Actor::Actor(int a, int b, int w, int h, int c, vector<Actor*> actors)
{
	width = w;
	height = h;
	x = a;
	y = b;
	colID = c;
	actors.pushback(this);
}

int Actor::getX() {return x;}
int Actor::getY() {return y;}
int Actor::getWidth() {return width;}
int Actor::getHeight() {return height;}
int Actor::getColID() {return colID;}



int Actor::checkCollision(Actor *obj)
{
	if(obj->getColID() != colID)
	{
		if(x+width <= obj->getX())
			return 1;
		if(y+height <= obj->getY())
			return 1;
		if(x >= obj->getX() + obj->getWidth())
			return 1;
		if(y >= obj->getY() + obj->getHeight())
			return 1;
	}
	return 0;
}

Actor *Actor::checkAllCollisions(vector<Actor *> a)
{
	for(int i = 0; i<a.size(); i++)
		if(checkcollision(a[i]))
			return &a[i];
	return NULL;
}

void Actor::remove()
{
	//removes the current object (Kyle is looking into this)
}