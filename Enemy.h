#include<vector>
#include <iostream>
using namespace std;
#ifndef ENEMY_H
#define ENEMY_H
class Enemy //everything that needs to check collisions is an actor (enemies, projectiles)
{
	public:
		Enemy(int a, int b, int w, int h, int c, vector<Actor*>, int);
		virtual void act();
		void dealDamage(int);
		void getHealth();
		void setBurning();
		void setSlow();
	private:
		bool burned;
		int health;
		
};

#endif