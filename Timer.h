#include "SDL/SDL.h"
#include <string>
#include <sstream>

using namespace std;


#ifndef TIMER_H
#define TIMER_H


   class Timer {


	private:
		int startTicks;		//clock time when the timer was started
		int pausedTicks;	//number of ticks when the timer was paused

		bool paused;		//status of timer, whether it is paused or started
		bool started;

	public:
		Timer();		//initializes

		void start();		//clock actions
		void stop();
		void pause();
		void unpause();

		int get_ticks();	//gets timer's time

		bool is_started();	//checks timer status
		bool is_paused();
};


#endif
