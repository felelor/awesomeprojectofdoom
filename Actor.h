#include<vector>
#include <iostream>
using namespace std;
#ifndef ACTOR_H
#define ACTOR_H
class Actor //everything that needs to check collisions is an actor (enemies, projectiles)
{
	public:
		Actor(int a, int b, int w, int h, int c, vector<Actor*>);
		
	//getter and setter
		int getX();
		int getY();
		int getWidth();
		int getHeight();
		int getColID; //this will change based on what type the inherited class is

	//collision stuff...	
		int checkCollision(Actor *);
		Actor *checkAllCollisions(vector<Actor *>); //returns the colID of the first object that registered a hit on you
		virtual void act() = 0; // will be overwritten to specify what each actor does when the loop is run (move, checkcollisions, react should be the order)
	private:
		int x, y, width, height, colID;
		
};

#endif