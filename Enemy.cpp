#include<vector>
#include <iostream>
#include <Actor.h>
using namespace std;

Enemy::Enemy(int a, int b, int w, int h, vector<Actor*> list, int he):Actor(a,b,w,h,list)
{
	burned = false;
	burnTimer = 0;
	slowed = false;
	slowTimer = 0;
	health = he;
}

void Enemy::act(vector<Actor*> list)//list All actors
{
	//move methods concerning images go here
	Actor *hit = checkAllCollisions(list); //grab collisions

	//reaction information
	if(hit->getColID == 4) //example if hits fire projectile w/ collision ID 4
	{
		burned = true;
		dealDamage(hit->getDamage());
		burnTimer = 50;
	}
	else if(hit->getColID == 8) //icebeam/water weapon/razorleaf wraps around?
	{
		slowed = true;
		dealDamage(hit->getDamage());
		slowTimer = 50;
	}

	if(burnTimer > 0)
	{
		burnTimer--;
		dealDamage(4); //arbitrary number
		if(burnTimer == 0)
			burned == false;
	}
	if(slowTimer>0)
	{
		slowTimer--;
//		curSpeed=speed/2;	not implemented and up for debate
		if(slowTimer == 0)
		{
			slowed == false;
//			curSpeed = maxSpeed	based on speed
		}
	}
	//we can add more as things go on
}
void Enemy::dealDamage(int amount) {health -=amount;}
void Enemy::getHealth() {return health;}
void Enemy::setBurning()
{
	burned = true;
	burnTimer = 50; 
}
void Enemey::setSlow()
{
	slowed = true;
	slowTimer = 50;
}