#include "Enemy.h"
using namespace std;
#ifndef MEOWTH_H
#define MEOWTH_H
/*
	Note:	These enemy files feed in specific variables to enemy parameters to make different types. They behave similarly so other methods are just 
		inherited from enemy
*/
class Meowth : public Enemy
{
	public:
		Meowth(int, int, Player*);
};
#endif
