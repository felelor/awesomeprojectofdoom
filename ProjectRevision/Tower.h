#include "Object.h"
#include "Projectile.h"
#include <iostream>
#include <vector>
#include <string>
using namespace std;


#ifndef TOWER_H
#define TOWER_H

class Tower : public Object{

	public:
		Tower(int,int,int,string,int,int,int,int);	//basic constructor feeds into object
		virtual string whatType();
		virtual void act(vector<Object *> &, vector<vector<Node> > &);
		virtual double dist(Object *);
		virtual int getBuy();
		virtual void setDeath(int);
		virtual int getDeath();
		virtual int getSell();
		virtual void setBuy(int);
		virtual void setSell(int);

		
	private:
		int buy;	//buy price
		int sell;	//sell price
		int cooldown;	//time between shots
		int range;	//range of projectile movement
};
#endif
