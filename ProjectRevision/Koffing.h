#include "Enemy.h"
using namespace std;
#ifndef KOFFING_H
#define KOFFING_H
/*
	Note:	These enemy files feed in specific variables to enemy parameters to make different types. They behave similarly so other methods are just 
		inherited from enemy
*/
class Koffing : public Enemy
{
	public:
		Koffing(int, int, Player*);
};
#endif
