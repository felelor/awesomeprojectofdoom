#include "Projectile.h"
#include "Object.h"
#include <string>
using namespace std;
#ifndef FIREPROJECTILE_H
#define FIREPROJECTILE_H
class FireProjectile : public Projectile
{
	public:
		FireProjectile(int, int,string,  Object *);
};
#endif

