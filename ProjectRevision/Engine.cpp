#include <SDL/SDL.h>
#include <SDL/SDL_ttf.h>
#include "Drawer.h"
#include "Node.h"
#include "Rattata.h"
#include "Arbok.h"
#include "Wheezing.h"
#include "Meowth.h"
#include "Raticate.h"
#include "Ekans.h"
#include "Koffing.h"
#include "Engine.h"
#include "Tile.h"
#include "Object.h"
#include "Player.h"
#include "Enemy.h"
#include "Tower.h"
#include "Button.h"
#include <string>
#include <vector>
#include <fstream>
#include <sstream>
#include <cstdlib>
#include <iomanip>
#include <cstdio>
#include <iostream>
#include <algorithm>
#include <ctime>
const int TILES_X = 20;	//global variables specifying tile dimensions and basic variables
const int TILES_Y = 20;
const int TILE_W = 40;
const int FPS = 25;
const int ENEMY_TYPES = 7;
const int MAXLEVEL = 16;
using namespace std;

Engine::~Engine() //clears all objects on board
{
	while(objs.size()>0)
		objs[0]->remove(objs);
}
void Engine::play()	// main function that runs the game
{
	srand(unsigned (time(0)));
	sum=1000;
	end_type=0;
	maper = 0; // lets you know when time for a new map
	Drawer disp = Drawer(TILES_X, TILES_Y, "Pokemon Tower Defense");
	trainer = &Player(10, 220);
	vector<int> temp = readfile("map.txt");		//reads in initial files for enemies and map
	vector<int> temp2 = readfile("enemies.txt");
	tiles = setTiles(temp);	//sets based on previous arrays
	nodes = setNodes(temp);
	//objs.push_back(new Tower(400,120,40,"tower",50,80));
	quit = false;
	enemiesSet = false;
	level = -1;
	counter = 0;
	stringstream ss;
	stringstream ss1;
	string map_str;
	string enem_str;
	int difference=0;
	int gotime=0;
	int movelevel=0;
	counter = 0;
	mapset = true;
	while(!quit)
	{	
		Uint32 startTime = SDL_GetTicks();	//starts timer for smooth animation
		if(waiting.size()==0){	//if there are no enemies waiting, allows enemies to be set
			enemiesSet = false;
			}
		movelevel=1;	//1 if there are enemies still in the array
		for(int i = 0; i< objs.size(); i++)	//for and if that set movelevel
		{
			if(objs[i]->getOType()==2 || objs[i]->getOType()==4 ){
				movelevel=0;
			}
		}
		if(level==int(temp2.size()/ENEMY_TYPES) && movelevel==1){	//checks movelevel and level to see if more enemies need to be waiting
			gotime=1;
		}
		
		if(gotime==1){	//sets enemies and map for a new level
			ss.str(string());	//concatenating strings for the readfile()
			ss1.str(string());
			ss.clear();
			ss1.clear();
			level=0;
			maper++;
			if(maper==MAXLEVEL){
				quit = true;
				end_type=1;
				break;
			}
			ss << maper;
			map_str = string("map")+ss.str()+string(".txt");
			ss1 << maper;
			enem_str = string("enemies")+ss.str()+string(".txt");
			temp.clear();	//clears all vectors each level
			waiting.clear();
			for(int i=0; i<nodes.size();i++){
				nodes[i].clear();
				}
			nodes.clear();
			tiles.clear();
			temp2.clear();
			objs.clear();
			for(int i=0; i<startlocs.size();i++){
				startlocs[i].clear();
				}
			startlocs.clear();	
			temp = readfile(map_str);	//reads new level data and sets initial conditions
			temp2 = readfile(enem_str);
			tiles = setTiles(temp);
			nodes = setNodes(temp);
			trainer->setMoney(220);
			SDL_Delay(2000);
			gotime=0;
			//trainer->setMoney(500);
			for(int i = 0; i< objs.size(); i++)		//sets initial buy and sell of the towers
				{
				    if(objs[i]->getOType()==7)
					{
						objs[i]->setPicName("tower");
						objs[i]->setBuy(0);
						objs[i]->setSell(0);
						
						}		
				   }
			mapset = true;
			}
				
			
		if(!enemiesSet && waiting.size()==0 && movelevel==1 && mapset)	//increases the level value and sets enemies each level
		{
			if(level!=int(temp2.size()/ENEMY_TYPES)){
				level++;
				}
			waiting = setEnemies(temp2);
			//cout<<"The new enemies in all actuality are: "<<waiting.size()<<endl;
			enemiesSet = true;
		}
		if(counter%(FPS*1)==0 && waiting.size()>0)	//moves objects from waiting vector to obj vector, making them display and interact
		{
			Object *baby = waiting[waiting.size()-1];
			waiting.pop_back();
			//cout<<"New enemy size is: "<<waiting.size()<<endl;
			objs.push_back(baby);
		}
		for(int i = 0; i< objs.size(); i++)	//calls the act method of all objects, inherited polymorphically to allow them all to move properly
		{
			objs[i]->act(objs, nodes);
			
		}	
		disp.DrawBoard(tiles, objs,trainer->getMoney(),(maper+1),trainer->getLives(),((temp2.size()/ENEMY_TYPES)-level));	//calls the draw method in the Drawer, drawing the entire board



		while (SDL_PollEvent(&action)) //our pause for clicking
		{
         		if (action.type == SDL_QUIT)
			{ //if x out is pressed close screen
				end_type = 2;
            			quit = true;
         		}
			else if(action.type == SDL_MOUSEBUTTONDOWN)	//if they clicked
			{
			    if(action.button.button == SDL_BUTTON_LEFT && trainer->getMoney()>=0){ //if it was a left click and they have money
				for(int i = 0; i< objs.size(); i++)
				{
				    if(objs[i]->isClicked(action.button.x,action.button.y)){	//if a certain object is clicked, sell them a tower
					//cout<<objs[i]->getBuy()<<endl;
					//cout<<trainer->getMoney()<<endl;
					//cout<<(trainer->getMoney()-(objs[i]->getBuy()+150))<<endl;
					
				    	if(objs[i]->getBuy()==0){
						difference=100;
						}	
					else{
						difference=150;
						}
					//cout<<"Difference is: "<<difference<<endl;
				    	if(objs[i]->getOType()==7 && (trainer->getMoney()-(objs[i]->getBuy()+difference))>=0){	//if it was a tower and they can buy it, set all pics accordingly based on pokemon type
						if(objs[i]->getPicName()=="charmander"){
							objs[i]->setPicName("charmeleon");
							objs[i]->setBuy(270);
							}
						else if(objs[i]->getPicName()=="charmeleon"){
							objs[i]->setBuy(420);
							objs[i]->setPicName("charizard");
							}
						else if(objs[i]->getPicName()=="charizard"){
							objs[i]->setBuy(0);
							objs[i]->setPicName("charizard");
							}
						else if(objs[i]->getPicName()=="bulbasaur"){
							objs[i]->setBuy(250);
							objs[i]->setPicName("ivysaur");
							}
						else if(objs[i]->getPicName()=="ivysaur"){
							objs[i]->setBuy(400);
							objs[i]->setPicName("venasaur");
							}
						else if(objs[i]->getPicName()=="venasaur"){
							objs[i]->setBuy(0);
							objs[i]->setPicName("venasaur");
							}
						else if(objs[i]->getPicName()=="squirtle"){
							objs[i]->setBuy(260);
							objs[i]->setPicName("warturtle");
							}
						else if(objs[i]->getPicName()=="warturtle"){
							objs[i]->setBuy(410);
							objs[i]->setPicName("blastoise");
							}
						else if(objs[i]->getPicName()=="blastoise"){
							objs[i]->setBuy(170);
							objs[i]->setPicName("blastoise");
							}
						else if (objs[i]->getPicName()=="tower"){
							string evol = objs[i]->whatType();//find the type of the object through user selection
							if(evol!="hole"){
							  objs[i]->setPicName(evol);
							  if(evol=="charmander"){	//buys the specified tower
								objs[i]->setBuy(120);
								}
							  else if(evol=="bulbasaur"){
								objs[i]->setBuy(100);
								}
							  else if(evol=="squirtle"){
								objs[i]->setBuy(110);
								}
								}
						        if((trainer->getMoney()-objs[i]->getBuy())<0){
							     objs[i]->setPicName("tower");
							     objs[i]->setBuy(0);
								}
							}
						trainer->setMoney((trainer->getMoney()-objs[i]->getBuy()));
						objs[i]->setSell((objs[i]->getBuy())/2);
						}
					}
				}
			     }
			    else if(action.button.button == SDL_BUTTON_RIGHT){ //if it's a right click
				for(int i = 0; i< objs.size(); i++)
				{
				    if(objs[i]->isClicked(action.button.x,action.button.y)){ //if object i was clicked
				    	if(objs[i]->getOType()==7 && objs[i]->getPicName()!="tower"){	//and it's a tower
						trainer->setMoney((trainer->getMoney()+objs[i]->getSell()));	//sell the tower
						objs[i]->setBuy(0);
						objs[i]->setPicName("tower");
						}
					}
				   }
				}
			}
		}
		if(trainer->getLives()==0){	//quits the game if the player loses all lives
			SDL_Delay(2000);
			quit = true;
			}
		counter++;
		Uint32 stopTime = SDL_GetTicks();//gets ticks and runs the delay
		
		if(stopTime-startTime < 1000/FPS)
			SDL_Delay(1000/FPS-(stopTime-startTime));

	}
	disp.Draw_End(0,0,0,0,1200,800,end_type);//draws the gameoverscreen
}

vector<Tile> Engine::setTiles(vector<int> &readin) //sets all the tiles and towers on the board
{
	
	vector<Tile> these;
	for(int i = 0; i<readin.size(); i++)
	{
		Tile *cur;
		if(readin[i]==0||readin[i]==7||readin[i]==6)				//if the tile shoudl be grass
			cur = &Tile(TILE_W, 0, (i%TILES_X)*TILE_W, (i/TILES_X)*TILE_W);
		else //if(readin[i]>0 && readin[i] <6)					//otherwise it's path
			cur = &Tile(TILE_W, 1, (i%TILES_X)*TILE_W, (i/TILES_X)*TILE_W);
		these.push_back(*cur);
		if(readin[i]==7)	//if it is a 7 push a left facing tower into objs
			objs.push_back(new Tower((i%TILES_X)*TILE_W, (i/TILES_X)*TILE_W,40,"tower",50,0,0,0));
		else if(readin[i]==6)	//if 6 push a right facing tower
			objs.push_back(new Tower((i%TILES_X)*TILE_W, (i/TILES_X)*TILE_W,40,"tower",50,0,0,1));
	}
	return these;
}
vector<vector<Node> > Engine::setNodes(vector<int> &readin)	//sets Nodes for pathfinding
{
	vector<vector<Node> > value;
	for(int i = 0; i<TILES_X; i++ )		//creates 2D node array with pathnumbers
	{
		vector<Node> thisrow;
		for(int j = 0; j<TILES_Y; j++)
		{
			Node a = Node(i, j, readin[TILES_Y*i+j]);
			thisrow.push_back(a);
		}
		value.push_back(thisrow);
	}
	for(int a = 0; a<value.size(); a++)
	{
		for(int b = 0; b<value[a].size(); b++)
		{
			//cout << "Looking at Node "<<value[a][0].getX()<<", "<<value[a][0].getY()<<endl;
			if(value[b][a].getPathNum()==4)		//searches for beginning tiles (marked by a 4), and sets the paths from them. see Node
			{
				value[b][a].cascadeNext(value);
				vector<int> oneloc;
				oneloc.push_back(a*TILE_W);
				oneloc.push_back(b*TILE_W);
				startlocs.push_back(oneloc);
			}
		}
	}
	/*for(int b = 0; b<TILES_X; b++) //a certain scientific printer
	{
		for(int c = 0; c<TILES_Y; c++)
		{
			cout<<value[b][c].getDir()<<" ";
		}
		cout<<endl;
	}*/
	return value;
}
vector<Object *> Engine::setEnemies(vector<int> &readin) //reads enemy amounts into the waiting vector
{
	vector<int> thiswave;
	for(int i = 0; i < ENEMY_TYPES; i++){
		//cout<<"level is: "<<level<<endl;
		thiswave.push_back(readin[level*ENEMY_TYPES+i]);	//grabs as many numbers as there are enemy types (with level determining which group)
		}

	vector<Object *> value;
	//cout<<"this wave of zero is: "<<thiswave[0]<<endl;
	for(int i = 0; i<thiswave[0]; i++) //first number represents Rattata, second Koffing, ... 
	{
		int k = rand()%startlocs.size();
		value.push_back(new Rattata(startlocs[k][0], startlocs[k][1], trainer));
	}
	for(int j = 0; j<thiswave[1]; j++)
	{
		int k = rand()%startlocs.size();
		value.push_back(new Koffing(startlocs[k][0], startlocs[k][1], trainer));
	}
	for(int j = 0; j<thiswave[2]; j++)
	{
		int k = rand()%startlocs.size();
		value.push_back(new Ekans(startlocs[k][0], startlocs[k][1], trainer));
	}
	for(int j = 0; j<thiswave[5]; j++)
	{
		int k = rand()%startlocs.size();
		value.push_back(new Arbok(startlocs[k][0], startlocs[k][1], trainer));
	}
	for(int j = 0; j<thiswave[3]; j++)
	{
		int k = rand()%startlocs.size();
		value.push_back(new Raticate(startlocs[k][0], startlocs[k][1], trainer));
	}
	for(int j = 0; j<thiswave[4]; j++)
	{
		int k = rand()%startlocs.size();
		value.push_back(new Wheezing(startlocs[k][0], startlocs[k][1], trainer));
	}
	for(int j = 0; j<thiswave[6]; j++)
	{
		int k = rand()%startlocs.size();
		value.push_back(new Meowth(startlocs[k][0], startlocs[k][1], trainer));
	}
	random_shuffle(value.begin(), value.end());
	return value;
}
vector<int> Engine::readfile(string filename)	//reads in a file with a specified name
{
	ifstream map;
	map.open(filename.c_str());	//opens the file
	if(map==NULL)	//error checks
	{
		//cout<<"You win!"<<endl;
		//quit = true;
		cout << "Error, file: "<<filename<<" not read"<<endl;
		exit(EXIT_FAILURE);
	}
	vector<int> temp;
	while(!map.eof())	//reads everything and returns it
	{ //find size of input file by plugging values into a vector and use get size. Then use a nested for loop to make board
		int a;
		map >> a;
		temp.push_back(a);
	}
	map.close();
	return temp;
}

void Engine::setSum(int s)	{sum=s;}
