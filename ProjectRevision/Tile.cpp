#include "Tile.h"
using namespace std;
/*
	Note:	This class stores information for easy tile reading by the drawer, so it does not contain any actions beyond getting and setting
*/
Tile::Tile(int w, int t, int a, int b)
{
	x = a;
	y = b;
	width = w;
	type = t;
}

int Tile::getX()	{return x;}
int Tile::getY()	{return y;}
int Tile::getType()	{return type;}
int Tile::getWidth()	{return width;}
