#include "SDL/SDL.h"
#include "Drawer.h"
#include "Node.h"
#include "Tile.h"
#include "Player.h"
#include "Object.h"
#include <string>
#include <vector>
#include <fstream>
#include <cstdlib>
#include <iomanip>
#include <cstdio>
#include <iostream>
using namespace std;
#ifndef ENGINE_H
#define ENGINE_H
class Engine
{
	public:
		~Engine();
		void play();	//runs the game
		vector<Tile> setTiles(vector<int> &);	//sets the Tiles based on vector from a file
		vector<vector<Node> > setNodes(vector<int> &);	//sets nodes based on vector above
		vector<Object *> setEnemies(vector<int> &);	//sets enemies based on a different vector from a different file
		vector<int> readfile(string); 	//reads in a file with the name as a parameter
		void setSum(int);

	private:
		bool quit;	//determines if the game should be quit
		int maper;	//determines levels
		int sum;
		int end_type;
		vector<Object *> objs;	//holds all objects currently active on the screen
		vector<Object *> waiting;	//holds all enemies waiting to be moved to objs
		vector<vector<Node> > nodes;	//2D vector of Nodes for pathfinding
		vector<Tile> tiles;	//vector of all the timers on the board
		SDL_Event action;	//SDL Event for clicking
		vector<vector<int> > startlocs;	//2D vector of all starting locations for the enemies
		int level;
		bool enemiesSet;	//true if the enemies have been set
		bool mapset;	//true if the timer has been set
		int counter;	//counts number of frames the game has run
		Player *trainer;	//Player character with lives and money amounts 
};
#endif
