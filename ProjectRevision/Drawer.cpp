#include <SDL/SDL.h>
#include <SDL/SDL_ttf.h>
#include <string>
#include <vector>
#include <sstream>
#include "Tile.h"
#include "Drawer.h"
#include <iostream>
#include "Object.h"
using namespace std;


Drawer::Drawer(int width, int height, const char* name) //deals with all drawing and actually holds the board
{
	//cout << "Here"<<endl;
	SDL_Init(SDL_INIT_EVERYTHING);	//initiates engine
	TTF_Init();	//initiates fonts
	screen = SDL_SetVideoMode(width*40+400, height*40, 32, SDL_SWSURFACE);	//initiates board
	SDL_WM_SetCaption(name, 0);	//sets name of game
	tilepics = SDL_LoadBMP("better_tiles.bmp");	//the many images we read in for graphics
	dot = SDL_LoadBMP("koffing.bmp");
	sprites = SDL_LoadBMP("sprites_new.bmp");
	fire = SDL_LoadBMP("fire.bmp");
	end_win = SDL_LoadBMP("end_win.bmp");
	end_lose = SDL_LoadBMP("end_lose.bmp");
	charmander = SDL_LoadBMP("charmduo.bmp");
	charmeleon = SDL_LoadBMP("charmeleon.bmp");
	charizard = SDL_LoadBMP("charizard.bmp");
	bulbasaur = SDL_LoadBMP("bulbasaur.bmp");
	ivysaur = SDL_LoadBMP("ivysaur.bmp");
	venusaur = SDL_LoadBMP("venasaur.bmp");
	squirtle = SDL_LoadBMP("squirtle.bmp");
	wartortle = SDL_LoadBMP("wartortle.bmp");
	blastoise = SDL_LoadBMP("blastoise.bmp");
	end = SDL_LoadBMP("end.bmp");
	raticate = SDL_LoadBMP("raticate.bmp");
	ekans = SDL_LoadBMP("ekans.bmp");
	wheezing = SDL_LoadBMP("wheezing.bmp");
	meowth = SDL_LoadBMP("meowth.bmp");
	koffing = SDL_LoadBMP("koffing.bmp");
	arbok = SDL_LoadBMP("arbok.bmp");
	meowth = SDL_LoadBMP("meowth.bmp");
	water = SDL_LoadBMP("water.bmp");
	leaf = SDL_LoadBMP("leaf.bmp");
	menu = SDL_LoadBMP("black_box.bmp");
	font = TTF_OpenFont( "OptimusPrincepsSemiBold.ttf", 30 );
	rattata = SDL_LoadBMP("rattata.bmp");
	SDL_SetColorKey( sprites, SDL_SRCCOLORKEY, SDL_MapRGB(sprites->format, 255, 0, 255) );	//sets all magenta (255, 0, 255) to transparent
	SDL_SetColorKey( dot, SDL_SRCCOLORKEY, SDL_MapRGB(dot->format, 255, 0, 255) );
	SDL_SetColorKey( fire, SDL_SRCCOLORKEY, SDL_MapRGB(fire->format, 255, 0, 255) );
	SDL_SetColorKey( water, SDL_SRCCOLORKEY, SDL_MapRGB(water->format, 255, 0, 255) );
	SDL_SetColorKey( leaf, SDL_SRCCOLORKEY, SDL_MapRGB(leaf->format, 255, 0, 255) );
	SDL_SetColorKey( tilepics, SDL_SRCCOLORKEY, SDL_MapRGB(tilepics->format, 255, 0, 255) );
	SDL_SetColorKey( rattata, SDL_SRCCOLORKEY, SDL_MapRGB(rattata->format, 255, 0, 255) );
	SDL_SetColorKey( koffing, SDL_SRCCOLORKEY, SDL_MapRGB(koffing->format, 255, 0, 255));
	SDL_SetColorKey( meowth, SDL_SRCCOLORKEY, SDL_MapRGB(meowth->format, 255, 0, 255));
	SDL_SetColorKey( arbok, SDL_SRCCOLORKEY, SDL_MapRGB(arbok->format, 255, 0, 255));
	SDL_SetColorKey( charmander, SDL_SRCCOLORKEY, SDL_MapRGB(charmander->format, 255, 0, 255));
	SDL_SetColorKey( charmeleon, SDL_SRCCOLORKEY, SDL_MapRGB(charmeleon->format, 255, 0, 255));
	SDL_SetColorKey( charizard, SDL_SRCCOLORKEY, SDL_MapRGB(charizard->format, 255, 0, 255));
	SDL_SetColorKey( bulbasaur, SDL_SRCCOLORKEY, SDL_MapRGB(bulbasaur->format, 255, 0, 255));
	SDL_SetColorKey( ivysaur, SDL_SRCCOLORKEY, SDL_MapRGB(ivysaur->format, 255, 0, 255));
	SDL_SetColorKey( venusaur, SDL_SRCCOLORKEY, SDL_MapRGB(venusaur->format, 255, 0, 255));
	SDL_SetColorKey( squirtle, SDL_SRCCOLORKEY, SDL_MapRGB(squirtle->format, 255, 0, 255));
	SDL_SetColorKey( wartortle, SDL_SRCCOLORKEY, SDL_MapRGB(wartortle->format, 255, 0, 255));
	SDL_SetColorKey( blastoise, SDL_SRCCOLORKEY, SDL_MapRGB(wartortle->format, 255, 0, 255));
	SDL_SetColorKey( raticate, SDL_SRCCOLORKEY, SDL_MapRGB(raticate->format, 255, 0, 255));
	SDL_SetColorKey( ekans, SDL_SRCCOLORKEY, SDL_MapRGB(ekans->format, 255, 0, 255));
	SDL_SetColorKey( wheezing, SDL_SRCCOLORKEY, SDL_MapRGB(wheezing->format, 255, 0, 255));
	SDL_SetColorKey( meowth, SDL_SRCCOLORKEY, SDL_MapRGB(meowth->format, 255, 0, 255));

	tclip[0].x = 0;	// sets general clips
	tclip[0].y = 0;
	tclip[0].w = 40;
	tclip[0].h = 40;

	tclip[1].x = 40;
	tclip[1].y = 0;
	tclip[1].w = 40;
	tclip[1].h = 40;

	tclip[2].x = 80;
	tclip[2].y = 0;
	tclip[2].w = 40;
	tclip[2].h = 40;

	tclip[7].x = 80;
	tclip[7].y = 0;
	tclip[7].w = 40;
	tclip[7].h = 40;

	tclip[18].x = 0;     // menu
	tclip[18].y = 0;
	tclip[18].w = 400;
	tclip[18].h = 800;
	
	for(int i = 0; i < 12; i++)	//sets clips for enemies
	{
		eclip[i].x = 40 * i;
		eclip[i].y = 0;
		eclip[i].w = 40;
		eclip[i].h = 40;
	}
	for(int j = 0; j < 3; j++)	//sets clips for projectiles
	{
		pclip[j].x = 20 * j;
		pclip[j].y = 0;
		pclip[j].w = 20;
		pclip[j].h = 20;
	}
		
	SDL_Color textColor = { 255, 255, 255 };	//sets text to white
	old_sum=0;

	message1 = TTF_RenderText_Solid( font, "Pokemon TD", textColor );	//displays name of game



}
Drawer::~Drawer()	//frees all surfaces and quits SDL/TTF
{
	SDL_FreeSurface(tilepics);
	SDL_FreeSurface(screen);
	SDL_FreeSurface(dot);
	SDL_FreeSurface(sprites);
	SDL_FreeSurface(message1);
	SDL_FreeSurface(message2);
	SDL_FreeSurface(menu);
	SDL_FreeSurface(fire);
	SDL_FreeSurface(charmander);
	SDL_FreeSurface(charmeleon);
	SDL_FreeSurface(charizard);
	SDL_FreeSurface(bulbasaur);
	SDL_FreeSurface(ivysaur);
	SDL_FreeSurface(venusaur);
	SDL_FreeSurface(squirtle);
	SDL_FreeSurface(wartortle);
	SDL_FreeSurface(blastoise);
	SDL_FreeSurface(raticate);
	SDL_FreeSurface(ekans);
	SDL_FreeSurface(wheezing);
	SDL_FreeSurface(meowth);
	SDL_FreeSurface(end_win);
	SDL_FreeSurface(end_lose);
	SDL_FreeSurface(end);
	SDL_FreeSurface(water);
	SDL_FreeSurface(leaf);
	SDL_FreeSurface(rattata);
	TTF_CloseFont( font );
	TTF_Quit();
	SDL_Quit();
}

void Drawer::DrawBoard(vector<Tile> &tiles, vector<Object *> &objects,int sum, int level,int lives,int wave)	//called by engine to draw the entire board
{

	DrawTiles(tiles);
	DrawObjects(objects);
	DrawText(sum,900,657);
	DrawText(level,910,705);
	DrawText(lives,1111,657);
	DrawText(wave,1111,705);
	SDL_Flip(screen);
}

void Drawer::DrawTiles(vector<Tile> &ts)	//draws tiles based on array
{
	for(int i = 0; i<ts.size(); i++)
	{
		apply_surface(ts[i].getX(), ts[i].getY(), tilepics, screen, &tclip[ts[i].getType()]);
	}
	apply_font(message1,screen,NULL,NULL,820,0, 100, 40);
}

void Drawer::DrawText(int s,int x, int y){	//draws text based on string and location
	SDL_Color textColor2 = { 255, 255, 255 };
	stringstream ss;
	ss << s;
	string str = ss.str();
	message2 = TTF_RenderText_Solid( font, str.c_str(), textColor2 );
	apply_font(message2,screen,NULL,NULL,x,y, 100, 40);
	}

void Drawer::apply_surface(int x, int y, SDL_Surface* source, SDL_Surface* destination, SDL_Rect* clip = NULL)	//applys a specified surface to a specified location
{
	SDL_Rect offset;
	offset.x = x;
	offset.y = y;
	//cout<<"made it"<<endl;
	if(SDL_BlitSurface(source, clip, destination, &offset)<0)
		cout << "Blit Failed: "<<SDL_GetError()<<endl;
}

void Drawer::DrawObjects(vector<Object *> &objs)	//draws all objects with their proper images
{
	for(int i = 0; i<objs.size(); i++)
	{
		if(objs[i]->getPicName()=="dot")
			apply_surface(objs[i]->getX(), objs[i]->getY(), dot, screen, &tclip[0]);
		else if(objs[i]->getPicName()=="ball")
			apply_surface(objs[i]->getX(), objs[i]->getY(), tilepics, screen, &tclip[2]);
		else if(objs[i]->getPicName()=="tower")
			apply_surface(objs[i]->getX(), objs[i]->getY(), tilepics, screen, &tclip[7]);
		else if(objs[i]->getPicName()=="charmander"){
			//apply_surface(objs[i]->getX(), objs[i]->getY(), sprites, screen, &tclip[8]);
			apply_surface(objs[i]->getX(), objs[i]->getY(), charmander, screen, &eclip[objs[i]->getClip()]);
			}
		else if(objs[i]->getPicName()=="charmeleon"){
			//apply_surface(objs[i]->getX(), objs[i]->getY(), sprites, screen, &tclip[9]);
			apply_surface(objs[i]->getX(), objs[i]->getY(), charmeleon, screen, &eclip[objs[i]->getClip()]);
			}
		else if(objs[i]->getPicName()=="charizard"){
			apply_surface(objs[i]->getX(), objs[i]->getY(), charizard, screen, &eclip[objs[i]->getClip()]);
			}
		else if(objs[i]->getPicName()=="bulbasaur"){
			apply_surface(objs[i]->getX(), objs[i]->getY(), bulbasaur, screen, &eclip[objs[i]->getClip()]);
			}
		else if(objs[i]->getPicName()=="ivysaur"){
			apply_surface(objs[i]->getX(), objs[i]->getY(), ivysaur, screen, &eclip[objs[i]->getClip()]);
			}
		else if(objs[i]->getPicName()=="venasaur"){
			apply_surface(objs[i]->getX(), objs[i]->getY(), venusaur, screen, &eclip[objs[i]->getClip()]);
			}
		else if(objs[i]->getPicName()=="squirtle"){
			apply_surface(objs[i]->getX(), objs[i]->getY(), squirtle, screen, &eclip[objs[i]->getClip()]);
			}
		else if(objs[i]->getPicName()=="warturtle"){
			apply_surface(objs[i]->getX(), objs[i]->getY(), wartortle, screen, &eclip[objs[i]->getClip()]);
			}
		else if(objs[i]->getPicName()=="blastoise"){
			apply_surface(objs[i]->getX(), objs[i]->getY(), blastoise, screen, &eclip[objs[i]->getClip()]);
			}
		else if(objs[i]->getPicName()=="raticate")
		{
			apply_surface(objs[i]->getX(), objs[i]->getY(), raticate, screen, &eclip[objs[i]->getClip()]);
		}
		else if(objs[i]->getPicName()=="ekans")
		{
			apply_surface(objs[i]->getX(), objs[i]->getY(), ekans, screen, &eclip[objs[i]->getClip()]);
		}
		else if(objs[i]->getPicName()=="wheezing")
		{
			apply_surface(objs[i]->getX(), objs[i]->getY(), wheezing, screen, &eclip[objs[i]->getClip()]);
		}

		else if(objs[i]->getPicName()=="meowth")
		{
			apply_surface(objs[i]->getX(), objs[i]->getY(), meowth, screen, &eclip[objs[i]->getClip()]);
		}
		else if(objs[i]->getPicName()=="fire"){
			apply_surface(objs[i]->getX(), objs[i]->getY(), fire, screen, &pclip[objs[i]->getClip()]);
			}
		else if(objs[i]->getPicName()=="water"){
			apply_surface(objs[i]->getX(), objs[i]->getY(), water, screen, &pclip[objs[i]->getClip()]);
			}
		else if(objs[i]->getPicName()=="leaf"){
			apply_surface(objs[i]->getX(), objs[i]->getY(), leaf, screen, &pclip[objs[i]->getClip()]);
			}
		else if(objs[i]->getPicName()=="rattata")
		{
			//cout<<objs[i]->getClip()<<endl;
			apply_surface(objs[i]->getX(), objs[i]->getY(), rattata, screen, &eclip[objs[i]->getClip()]);
		}
		else if(objs[i]->getPicName()=="koffing")
		{
			apply_surface(objs[i]->getX(), objs[i]->getY(), koffing, screen, &eclip[objs[i]->getClip()]);
		}
		else if(objs[i]->getPicName()=="meowth")
		{
			apply_surface(objs[i]->getX(), objs[i]->getY(), meowth, screen, &eclip[objs[i]->getClip()]);
		}
		else if(objs[i]->getPicName()=="arbok")
		{
			apply_surface(objs[i]->getX(), objs[i]->getY(), arbok, screen, &eclip[objs[i]->getClip()]);
		}

		/*else if(objs[i]->getPicName()=="button"){
			apply_surface(objs[i]->getX(), objs[i]->getY(), tilepics, screen, &tclip[9]);
			}*/
	}
	apply_surface(800, 0, menu, screen, &tclip[18]);	//displays menus
}

void Drawer::apply_font(SDL_Surface* picture,SDL_Surface* screen, int pic_X, int pic_Y, int screen_X, int screen_Y, int width, int height){	//applys fonts to a specified location

	SDL_Rect pic_Rect;
	pic_Rect.x = pic_X;
   	pic_Rect.y = pic_Y;
   	pic_Rect.w = width;
   	pic_Rect.h = height;

   	SDL_Rect screen_Rect;
   	screen_Rect.x = screen_X;
   	screen_Rect.y = screen_Y;
   	screen_Rect.w = width;
   	screen_Rect.h = height;
	
   	SDL_BlitSurface(picture, &pic_Rect, screen, &screen_Rect);

}

void Drawer::Draw_End(int p_x,int p_y,int s_x,int s_y,int w,int h,int et){	//draws the game over screens
	if(et==0){	//lose
		apply_font(end_lose,screen,p_x,p_y,s_x,s_y,w,h);
		}
	else if(et==1){	//win
		apply_font(end_win,screen,p_x,p_y,s_x,s_y,w,h);
		}
	else if(et==2){	//standard
		apply_font(end,screen,p_x,p_y,s_x,s_y,w,h);
		}
	SDL_Flip(screen);
	SDL_Delay(2000);
}

