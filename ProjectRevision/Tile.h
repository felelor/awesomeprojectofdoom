#ifndef TILE_H
#define TILE_H
/*
	Note:	This class stores information for easy tile reading by the drawer, so it does not contain any actions beyond getting and setting
*/
class Tile
{
	public:
		Tile(int, int, int, int);
		int getX();
		int getY();
		int getType();
		int getWidth();
	private:
		int x;
		int y;
		int type;
		int width;
};

#endif
