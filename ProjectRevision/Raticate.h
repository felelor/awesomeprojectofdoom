#include "Enemy.h"
using namespace std;
#ifndef RATICATE_H
#define RATICATE_H
/*
	Note:	These enemy files feed in specific variables to enemy parameters to make different types. They behave similarly so other methods are just 
		inherited from enemy
*/
class Raticate : public Enemy
{
	public:
		Raticate(int, int, Player*);
};
#endif
