#include "Engine.h"
#include <SDL/SDL.h>
#include <iostream>
using namespace std;

int main(int argc, char **argv)
{
	Engine game;
	game.play();
	return 0;
}
