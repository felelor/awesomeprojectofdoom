#include "Enemy.h"
using namespace std;
#ifndef EKANS_H
#define EKANS_H
/*
	Note:	These enemy files feed in specific variables to enemy parameters to make different types. They behave similarly so other methods are just 
		inherited from enemy
*/
class Ekans : public Enemy
{
	public:
		Ekans(int, int, Player*);
};
#endif
