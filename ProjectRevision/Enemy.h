#include "Node.h"
#include "Object.h"
#include "Player.h"
#include <iostream>
#include <vector>
#include <string>
using namespace std;
/*
Note:	This is an abstract class. There should be no instances of object created and many of its functions are empty to allow for polymorphic 
	functioning higher in the hierarchy
*/
#ifndef ENEMY_H
#define ENEMY_H
class Enemy : public Object
{
	public:
		Enemy(int, int, int, double, string, int,int, Player*, int, int);
		virtual void act(vector<Object *> &, vector<vector<Node> > &);
		virtual void dealDamage(int);
		virtual void setDeath(int);
		virtual int getDeath();
		virtual void react(Object *, vector<Object *> &);
		virtual void animate();
	private:
		double vel;	//constant velocity of enemies
		int shift;	//amount in between image switching
		char dir;	//direction currently moving
		int gold;	//reward for killing
		int death;
		int health;	//health of the enemy
		int holdType;	//helps in animation
		Player*target;	//Player passed by engine to allow enemies to deal damage
};
#endif
