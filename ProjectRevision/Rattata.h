#include "Enemy.h"
using namespace std;
#ifndef RATTATA_H
#define RATTATA_H
/*
	Note:	These enemy files feed in specific variables to enemy parameters to make different types. They behave similarly so other methods are just 
		inherited from enemy
*/
class Rattata : public Enemy
{
	public:
		Rattata(int, int, Player*);
};
#endif
