#include "Tower.h"
#include "Object.h"
#include "Projectile.h"
#include <cmath>
#include <iostream>
#include <vector>
#include <string>

using namespace std;

const int BULB_CD = 75;	//constants for cooldowns and range
const int CHAR_CD = 13;
const int SQUI_CD = 25;
const int CHAR_R = 120;
const int BULB_R = 240;
const int SQUI_R = 180;
Tower::Tower(int a, int b, int c, string image,int h,int p, int s,int t):Object(a,b,c,0,0,image,7,t){

	buy = p;
	sell = s;
	cooldown = 0;
	range = 100000;	//high range for initial tower (allows it to be set later)
}
//getters and setters
void Tower::setDeath(int)	{}
int Tower::getDeath()	{}

int Tower::getBuy() {return buy;}
int Tower::getSell() {return sell;}
void Tower::setBuy(int bu) {buy = bu;}
void Tower::setSell(int se) {sell = se;}

string Tower::whatType(){	//checks what type the tower should become (first evolution)
	SDL_Event key_test;
	while(1){
		while(SDL_PollEvent(&key_test)){	//checks for event
			if(key_test.type == SDL_KEYDOWN){	//ifany key is pressed
				switch(key_test.key.keysym.sym){
					case SDLK_q:	//quit
						return "hole";
						break;
					case SDLK_c:	//charmander
						return "charmander";
						break;
					case SDLK_b:	//bulbasaur
						return "bulbasaur";
						break;
					case SDLK_s:	//squirtle
						return "squirtle";
						break;
					}
				}
			}
		}
	}

void Tower::act(vector<Object *> &objs, vector<vector<Node> > &nodes)	//act method
{
	Object * closest = NULL;	//target it may shoot at
	double distance = 0;
	if(pic == "charmander" || pic == "charmeleon" || pic == "charizard")	//sets range for the frame
		range = CHAR_R;
	else if(pic == "squirtle" || pic == "warturtle" || pic == "blastoise")
		range = SQUI_R;
	else if(pic == "bulbasaur" || pic == "ivysaur" || pic == "venasaur")
		range = BULB_R;
	for(int i = 0; i<objs.size(); i++)	//looks for closest object
	{
		double newdist = objs[i]->getTimeAlive()*objs[i]->getSpeed();
		if(objs[i]->getColID()==2 && dist(objs[i])<range&&newdist>distance)	//if it finds an enemy closer than the previous ones
		{
			distance = newdist;
			closest = objs[i];	//sets closest
		}
	}

	
	if(closest!=NULL&&cooldown<=0)
	{
		if(pic == "charmander" || pic == "charmeleon" || pic == "charizard")	//charmander action
		{
			objs.push_back(new Projectile(location.x+10, location.y+10,7, range, "fire",closest));
			if(pic == "charmander")
				cooldown = CHAR_CD;
			else if(pic == "charmeleon")
				cooldown = CHAR_CD/2;
			else
				cooldown = CHAR_CD/3;
		}
		else if(pic == "squirtle" || pic == "warturtle" || pic == "blastoise")	//squirtle action
		{
			objs.push_back(new Projectile(location.x+10, location.y+10,7, range,"water", closest));
			if(pic == "squirtle")
				cooldown = SQUI_CD;
			else if(pic == "warturtle")
				cooldown = SQUI_CD/2;
			else
				cooldown = SQUI_CD/3;
		}
		else if(pic == "bulbasaur" || pic == "ivysaur" || pic == "venasaur")	//bulbasaur action
		{
			objs.push_back(new Projectile(location.x+10, location.y+10,7, range,"leaf", closest));
			if(pic == "bulbasaur")
				cooldown = BULB_CD;
			else if(pic == "ivysaur")
				cooldown = BULB_CD/2;
			else
				cooldown = BULB_CD/3;
		}
	}

	cooldown--;

}


double Tower::dist(Object *obj)	//calculates distance from the enemy (distance formula)
{
	return sqrt(pow(location.x - obj->getX(), 2)+pow(location.y - obj->getY(), 2));
}

