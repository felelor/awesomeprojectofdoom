//#include "SDL.h"
#include<vector>
#include <iostream>
using namespace std;
#ifndef NODE_H
#define NODE_H
class Node	//creates the Paths that enemies follow (up to 3)
{
	public:
		Node(int, int, int);
		int getPathNum();
		void setPathNum(int);
		int getY();
		int getX();
		void cascadeNext( vector<vector<Node> > &);
		Node *getNext();
		void setPrev(Node *);
		char getDir();
		char getType();
	private:
		int x;
		int y;
		int pathNum;	//read in for file
		Node *next;	//the next node along the path
		Node *prev;	//the previous node along the path
		char type;	//the type of node (beginning, path, end)
		char dir;	//direction the path will give
};


#endif
