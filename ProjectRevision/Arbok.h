#include "Enemy.h"
using namespace std;
/*
	Note:	These enemy files feed in specific variables to enemy parameters to make different types. They behave similarly so other methods are just 
		inherited from enemy
*/
#ifndef ARBOK_H
#define ARBOK_H

class Arbok : public Enemy
{
	public:
		Arbok(int, int, Player*);
};
#endif
