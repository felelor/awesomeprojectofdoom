#include "Rattata.h"
#include "Enemy.h"
#include "Player.h"
using namespace std;
/*
	Note:	These enemy files feed in specific variables to enemy parameters to make different types. They behave similarly so other methods are just 
		inherited from enemy
*/
Rattata::Rattata(int a, int b, Player* targ):Enemy(a, b, 40, 4, "rattata", 3, 0, targ, 5, 15)
{

}
