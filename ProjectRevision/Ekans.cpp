#include "Ekans.h"
#include "Enemy.h"
#include "Player.h"
using namespace std;
/*
	Note:	These enemy files feed in specific variables to enemy parameters to make different types. They behave similarly so other methods are just 
		inherited from enemy
*/
Ekans::Ekans(int a, int b, Player* targ):Enemy(a, b, 40, 1, "ekans", 6, 0, targ, 7, 15)
{

}
