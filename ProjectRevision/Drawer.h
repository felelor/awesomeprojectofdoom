#include <SDL/SDL.h>
#include <SDL/SDL_ttf.h>
#include <sstream>
#include <string>
#include <vector>
#include "Tile.h"
#include "Object.h"
using namespace std;

#ifndef DRAWER_H
#define DRAWER_H
class Drawer
{
	public:
		Drawer(int, int, const char*);	//constructer reading in tile dimensions and a name
		~Drawer();
		void DrawBoard(vector<Tile> &, vector<Object *> &,int,int,int,int);	//draws board
		void DrawTiles(vector<Tile> &);	//draws tiles
		void DrawText(int,int,int);	//draws all text areas
		void Draw_End(int,int,int,int,int,int,int);	//draws gameover screen
		void apply_surface(int, int, SDL_Surface *, SDL_Surface *, SDL_Rect*);	//applys specified surface to location
		void DrawObjects(vector<Object *> &);	//draws objects
		void apply_font(SDL_Surface*,SDL_Surface*,int,int,int,int,int,int);	//blits a font to the screen
	private:
		int old_sum;
		SDL_Rect tclip[21];	//clip arrays
		SDL_Rect eclip[12];
		SDL_Rect pclip[3];
		SDL_Surface *screen;	//all .bmp files
		SDL_Surface *tilepics;
		SDL_Surface *dot;
		SDL_Surface *end_win;
		SDL_Surface *end_lose;
		SDL_Surface *end;
		SDL_Surface *charmander;
		SDL_Surface *charmeleon;
		SDL_Surface *charizard;
		SDL_Surface *bulbasaur;
		SDL_Surface *ivysaur;
		SDL_Surface *venusaur;
		SDL_Surface *squirtle;
		SDL_Surface *wartortle;
		SDL_Surface *blastoise;
		SDL_Surface *sprites;
		SDL_Surface *fire;
		SDL_Surface *water;
		SDL_Surface *leaf;
		SDL_Surface *rattata;
		SDL_Surface *arbok;
		SDL_Surface *meowth;
		SDL_Surface *koffing;
		SDL_Surface* raticate;
		SDL_Surface* ekans;
		SDL_Surface* wheezing;
		SDL_Surface* message1;
		SDL_Surface* message2;
		//SDL_Surface* message3;
		SDL_Surface* menu;
		TTF_Font *font;

};

#endif
