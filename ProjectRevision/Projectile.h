#include "Object.h"
#include <string>
#include <vector>
using namespace std; 

#ifndef PROJECTILE_H
#define PROJECTILE_H
class Projectile : public Object
{
	public:
		Projectile(int, int, double, int,string, Object *);
		virtual void act(vector<Object *> &objs, vector<vector<Node> > &nodes);	//act method called in engine
		double dist(Object *obj);	//for calculations
		virtual void setDeath(int);
		virtual int getDeath();
		virtual int getDamage();
		void updateVel();	//updates velocities
		void animate();		//animates projectiles
	protected:
		int life;	//the range of the projectile
		double speed;	//the base speed the projectile is traveling
		Object *target;	//the projectile's target
		bool tseek;	//true if seeking target
		int damage;	//damage the projectile does
};

#endif
