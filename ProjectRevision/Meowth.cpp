#include "Meowth.h"
#include "Enemy.h"
#include "Player.h"
using namespace std;
/*
	Note:	These enemy files feed in specific variables to enemy parameters to make different types. They behave similarly so other methods are just 
		inherited from enemy
*/
Meowth::Meowth(int a, int b, Player* targ):Enemy(a, b, 40, 2, "meowth", 25, 0, targ, 5, 30)
{

}
