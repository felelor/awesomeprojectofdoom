#include "Enemy.h"
using namespace std;
#ifndef WHEEZING_H
#define WHEEZING_H
/*
	Note:	These enemy files feed in specific variables to enemy parameters to make different types. They behave similarly so other methods are just 
		inherited from enemy
*/
class Wheezing : public Enemy
{
	public:
		Wheezing(int, int, Player*);
};
#endif
