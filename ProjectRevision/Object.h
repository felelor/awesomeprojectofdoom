#include <SDL/SDL.h>
#include <string>
#include <vector>
#include "Node.h"
using namespace std;

#ifndef OBJECT_H
#define OBJECT_H
class Object
{
/*
Note:	This is an abstract class. There should be no instances of object created and many of its functions are empty to allow for polymorphic 
	functioning higher in the hierarchy. Virtual functions allow them to be adopted by classes that do not use them.
*/
	public:
		Object(int, int, int, double, double, string, int,int);	//constructer called by derived classes
		virtual int getX();	//getters and setters
		virtual int getY();
		virtual int getW();
		virtual int getBuy();
		virtual int getSell();
		virtual void setBuy(int);
		virtual void setSell(int);
		virtual int getOType();
		virtual int getColID();
		virtual int getTimeAlive();
		virtual int getDeath();
		virtual double getSpeed();
		virtual void setPicName(string);
		virtual void setDeath(int);
		virtual string getPicName();
		virtual SDL_Rect *getLoc();
		virtual void act(vector<Object *> &, vector<vector<Node> > &);	//act function called to move, overwritten by most classes
		virtual void remove(vector<Object *> &);	//removes object
		virtual int setW(int wid);
		virtual void checkCollision(vector<Object *> &);	//checks collisions with the entire vector 
		virtual bool isColliding(Object *);	//checks collisions with one object
		virtual void react(Object *, vector<Object *> &);	//reacts to an object
		virtual string whatType();	//tells what type of tower (used only by tower)
		virtual bool isClicked(int, int);	//retuns true if clicked
		virtual int getClip();
		virtual int getDamage();
	protected:	//protected to allow functionality with derived classes
		SDL_Rect location;
		string pic;
		double xvel;
		vector<Object*> dead_vec;
		double yvel;
		double xloc;
		double yloc;
		int colid;
		int type;
		int timeAlive;	//number of frames the object has been alive
};
#endif
