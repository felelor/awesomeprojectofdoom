#include "Projectile.h"
#include "Object.h"
#include "Tower.h"
#include <cmath>
#include <vector>
#include <string>
#include <iostream>
using namespace std;

Projectile::Projectile(int a, int b, double vel, int r,string name, Object *targ) : Object(a, b, 20, 0, 0, name, 4, 0)
{
	target = targ;	//target enemy 
	double hyp = dist(target);	//calculations for later		
	double rise = target->getY()-location.y;
	double run = target->getX()-location.x;
	if(name == "leaf")	//sets damage
		damage = 4;
	else if(name == "water")
		damage = 2;
	else if(name == "fire")
		damage = 1;
	xvel = (7 * run/hyp+.5);	//calculates xvel and yvel to allow angular motion
	yvel = (7 * rise/hyp+.5);
	life = r;	//the amount of frames it can last
	speed = vel;	//speed of the projectile
	tseek = true;	//true if seeking target
}
//getters
void Projectile::setDeath(int)	{}
int Projectile::getDeath()	{}
int Projectile::getDamage()	{return damage;}

void Projectile::act(vector<Object *> &objs, vector<vector<Node> > &nodes)	//act method
{
	if(timeAlive%5==0)	//animates projectiles
		animate();
	if(dist(target)<10)	//follows the target until within 10 pixels
		tseek = false;
	if(tseek)		//updates velocities if seeking
		updateVel();
	xloc += xvel;	//update location based on velocity
	yloc += yvel;
	location.x = (int) xloc;
	location.y = (int) yloc;
	if(life<timeAlive*speed)	//checks for range limit D=RT
		remove(objs);
	timeAlive++;	//keeps track of time alive for range calculation
}
double Projectile::dist(Object *obj)	//distance formula
{
	return sqrt(pow(location.x - obj->getX(), 2)+pow(location.y - obj->getY(), 2));
}
void Projectile::animate()	//switches image to allow animation
{
	if(type == 0)
		type = 1;
	else if(type = 1)
		type = 2;
	else if(type = 2)
		type = 0;
}
void Projectile::updateVel()	//updates velocity based on enemy location
{
	double hyp = dist(target);
	double rise = target->getY()-location.y;
	double run = target->getX()-location.x;
	xvel = (7 * run/hyp+.5);
	yvel = (7 * rise/hyp+.5);
}

