#include "Player.h"
using namespace std;

Player::Player(int h, int m)
{
	lives = h;
	money = m;
}


int Player::getLives()	{return lives;}
int Player::getMoney()	{return money;}
void Player::earnMoney(int a)	{money+=a;}
void Player::setMoney(int a)	{money=a;}
void Player::loseLives(int num)	{lives-=num;}
bool Player::spend(int num)	//possible redundancy due to miscommunication
{
	if(money<num)
		return false;
	money-=num;
	return true;
}

