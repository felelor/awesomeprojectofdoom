

using namespace std;
#ifndef PLAYER_H
#define PLAYER_H
class Player	//A class which stores and adapts the numbers representing the player
{
	public:
		Player(int, int);
		int getLives();
		int getMoney();
		void loseLives(int);
		void setMoney(int);
		void earnMoney(int);
		bool spend(int);

	private:
		int lives;
		int money;
};

#endif
