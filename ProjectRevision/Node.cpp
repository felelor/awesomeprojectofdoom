//#include "SDL.h"
#include "Node.h"
#include<vector>
#include <iostream>
using namespace std;
Node::Node( int setx, int sety, int path)
{
	x=setx;
	y=sety;
	pathNum = path;
	if(path == 4)	//path is a beginning
	{
		//cout<<"set "<<x<<", "<<y<<" to b"<<endl;
		type = 'b';
	}
	else if(path == 5)	//end
	{
		//cout<<"set "<<x<<", "<<y<<" to e"<<endl;
		type = 'e';
	}
	else if(path > 0 && path<=3)	//standard path
		type ='p';
	else	//unexpected
		type = 'n';
}

//getters
char Node::getType()	{return type;}
int Node::getX()	{return x;}
int Node::getY() {return y;}

Node *Node::getNext() {return next;}

void Node::cascadeNext(vector<vector<Node> > &board)	//creates the paths recursivly for enemies to follow
{
	//cout<<getY()<<" compared to "<<board.size()<<endl;
	//if(getY()==board.size()-1)
	if(type ==  'e')	//if it's an end node (base case)
	{
		dir = '0';
		pathNum = (*prev).getPathNum();
		return;
	}
	//cout<<board[x+1][y].getPathNum() <<endl;
	
	else if(type == 'b')	//if a beginning node
	{
		//in each state: gives the node a direction, sets its next node to the found node, sets that nodes previous to this, and calls cascadeNext on it
		if(x < board.size()-1 && board[x+1][y].getPathNum()>0)	//searches for path node down
		{
			//cout<<"found Node down"<<endl;
			dir = 'd';
			pathNum = board[x+1][y].getPathNum();
			next = &board[x+1][y];
			board[x+1][y].setPrev(this);
			board[x+1][y].cascadeNext(board);
		}
		else if(x > 0 && board[x-1][y].getPathNum()>0)	//seraches for path node up
		{
			dir = 'u';
			pathNum = board[x-1][y].getPathNum();
			next = &board[x-1][y];
			board[x-1][y].setPrev(this);
			board[x-1][y].cascadeNext(board);
		}
		else if(y < board.size()-1 && board[x][y+1].getPathNum()>0)	//searches for path node right
		{
			dir = 'r';
			pathNum = board[x][y+1].getPathNum();
			next = &board[x][y+1];
			board[x][y+1].setPrev(this);
			board[x][y+1].cascadeNext(board);
		}
		else if(y > 0 && board[x][y-1].getPathNum()>0)	//searches for path node left
		{
			dir = 'l';
			pathNum = board[x][y-1].getPathNum();
			next = &board[x][y-1];
			board[x][y-1].setPrev(this);
			board[x][y-1].cascadeNext(board);
		}
		else		//error check
			cout<<"Next Node not found"<<endl;
		return;
	}
	else if(type == 'p')	//if path node
	{
		/*checks for: 
			location (to prevent out of bounds calls) 
			similar or ending path nums(read in from file)
			the node's next to make sure there is no overlap 
		in each case: 
			sets direction
			sets next to found node
			sets that node's previous to this
			cascades the next*/
		if(x < board.size()-1 && (board[x+1][y].getPathNum() == pathNum||board[x+1][y].getPathNum()==5) && this != board[x+1][y].getNext())//down
		{
			dir = 'd';
			next = &board[x+1][y];
			board[x+1][y].setPrev(this);
//			cout<<"found next at "<<x+1<<", "<<y<<endl;
//			board[x+1][y].setPathNum(getPathNum()+1);
			board[x+1][y].cascadeNext(board);
			return;
		}
		if(y > 0 && (board[x][y-1].getPathNum() ==pathNum||board[x][y-1].getPathNum()==5) && this != board[x][y-1].getNext())	//left
		{
			dir = 'l';
			next = &board[x][y-1];
			board[x][y-1].setPrev(this);
//			cout<<"found next at "<<x<<", "<<y-1<<endl;
			board[x][y-1].cascadeNext(board);
			return;
		}
		if(y < board.size()-1&& (board[x][y+1].getPathNum() == pathNum||board[x][y+1].getPathNum() == 5) && this != board[x][y+1].getNext())//right
		{
			dir = 'r';
			next = &board[x][y+1];
			board[x][y-1].setPrev(this);
//			cout<<"found next at "<<x<<", "<<y+1<<endl;
			board[x][y+1].cascadeNext(board);
			return;
		}
		if(x > 0 && (board[x-1][y].getPathNum() == pathNum||board[x-1][y].getPathNum() == 5) && this != board[x-1][y].getNext())	//up
		{
			dir = 'u';
			next = &board[x-1][y];
			board[x-1][y].setPrev(this);
//			cout<<"found next at "<<x<<", "<<y+1<<endl;
			board[x-1][y].cascadeNext(board);
			return;
		}
	}
	cout<<"Error, could not find next node for node type: "<< type<< " at "<<x<<", "<<y<<endl;
	return;
}

//more getters and setters
void Node::setPathNum(int num) {pathNum = num;}

int Node::getPathNum()	{return pathNum;}

void Node::setPrev(Node *in) {prev = in;}

char Node::getDir() {return dir;}

