#include<vector>
#include <iostream>
#include <Actor.h>
#include <Enemy.h>
using namespace std;

FireProjectile::FireProjectile(int a, int b, int w, int h, vector<Actor*> list, int life, int d):Projectile(a,b,w,h,list, life)
{
	damage = d;
}
void FireProjectile::act(vector<Actor*> list)//list All actors
{
	//move methods concerning images go here
	Enemy *hit = checkAllCollisions(list); //grab collisions

	//reaction information
	if(hit->getColID == 2||time==0) //example if hits enemy (all enemies have same collision ID for )
	{
		*(hit).dealDamage(damage);
		remove();
	}
	time--;
	//we can add more as things go on
}

void FireProjectile::getDamamge {return damage;}
