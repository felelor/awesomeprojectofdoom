#include "Object.h"
#include <vector>
using namespace std; 

#ifndef PROJECTILE_H
#define PROJECTILE_H
class Projectile : public Object
{
	public:
		Projectile(int, int, double, double, int);
		virtual void act(vector<Object *> &objs, vector<vector<Node> > &nodes);
	protected:
		int life;
		double speed;
};

#endif
