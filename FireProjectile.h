#include<vector>
#include <iostream>
using namespace std;
#ifndef PROJECTILE_H
#define PROJECTILE_H
class FireProjectile
{
	public:
		FireProjectile(int a, int b, int w, int h, int c, vector<Actor*>, int, int);
		virtual void act();
		void getDamage();
	private:
		int damage;
};

#endif